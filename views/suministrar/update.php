<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Suministrar */

$this->title = 'Update Suministrar: ' . $model->proveedor_NIF;
$this->params['breadcrumbs'][] = ['label' => 'Suministrars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->proveedor_NIF, 'url' => ['view', 'proveedor_NIF' => $model->proveedor_NIF, 'producto_codigo' => $model->producto_codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suministrar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoProductos' => $listadoProductos,
        'listadoProveedores' => $listadoProveedores,
    ]) ?>

</div>
