<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suministrar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suministrar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Suministrar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'proveedor_NIF',
            'proveedorNIF.nombre',
            'producto_codigo',
            'productoCodigo.nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
