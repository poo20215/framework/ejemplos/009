<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Suministrar */

$this->title = 'Create Suministrar';
$this->params['breadcrumbs'][] = ['label' => 'Suministrars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suministrar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoProductos' => $listadoProductos,
        'listadoProveedores' => $listadoProveedores,
    ]) ?>

</div>
