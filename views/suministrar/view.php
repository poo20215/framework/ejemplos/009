<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Suministrar */

$this->title = $model->proveedor_NIF;
$this->params['breadcrumbs'][] = ['label' => 'Suministrars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="suministrar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'proveedor_NIF' => $model->proveedor_NIF, 'producto_codigo' => $model->producto_codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'proveedor_NIF' => $model->proveedor_NIF, 'producto_codigo' => $model->producto_codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'proveedor_NIF',
            'producto_codigo',
        ],
    ]) ?>

</div>
