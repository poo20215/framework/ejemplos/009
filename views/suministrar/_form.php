<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Suministrar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="suministrar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proveedor_NIF')->dropDownList($listadoProveedores) ?>

    <?= $form->field($model, 'producto_codigo')->dropDownList($listadoProductos) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
