<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comprar */

$this->title = $model->producto_codigo;
$this->params['breadcrumbs'][] = ['label' => 'Comprars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comprar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'producto_codigo' => $model->producto_codigo, 'cliente_DNI' => $model->cliente_DNI], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'producto_codigo' => $model->producto_codigo, 'cliente_DNI' => $model->cliente_DNI], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'producto_codigo',
            'cliente_DNI',
        ],
    ]) ?>

</div>
