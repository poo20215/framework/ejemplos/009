<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comprar */

$this->title = 'Update Comprar: ' . $model->producto_codigo;
$this->params['breadcrumbs'][] = ['label' => 'Comprars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->producto_codigo, 'url' => ['view', 'producto_codigo' => $model->producto_codigo, 'cliente_DNI' => $model->cliente_DNI]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="comprar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoProductos' => $listadoProductos,
        'listadoClientes' => $listadoClientes,
    ]) ?>

</div>
