<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comprar */

$this->title = 'Create Comprar';
$this->params['breadcrumbs'][] = ['label' => 'Comprars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comprar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoProductos' => $listadoProductos,
        'listadoClientes' => $listadoClientes,
    ]) ?>

</div>
