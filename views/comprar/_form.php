<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comprar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comprar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producto_codigo')->dropDownList($listadoProductos) ?>

    <?= $form->field($model, 'cliente_DNI')->dropDownList($listadoClientes) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
