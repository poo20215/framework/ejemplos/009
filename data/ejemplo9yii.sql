-- MySQL dump 10.13  Distrib 5.7.36, for Win64 (x86_64)
--
-- Host: localhost    Database: ejemplo9yii
-- ------------------------------------------------------
-- Server version	5.7.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `DNI` char(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(80) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_de_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES ('73451834Y','Jose Luis','García Navarro','Calle alta 18','1957-03-20'),('73561249D','Jorge','Lopez Ruiz','Calle el monte 134','1984-03-18'),('74894562J','Manuel','Rodriguez Jimenez','Avenida Los Castros 67','1968-11-09'),('78975976L','Patricia','Martinez Perez','Los rios 26','1996-08-21'),('79070685M','Antonio','Sanchez Gomez','Calle Castilla 18','1992-05-30'),('79372651P','Marina','Sancho Fernandez','Calle Burgos 47','1973-05-04');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprar`
--

DROP TABLE IF EXISTS `comprar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comprar` (
  `producto_codigo` varchar(20) NOT NULL,
  `cliente_DNI` char(9) NOT NULL,
  PRIMARY KEY (`producto_codigo`,`cliente_DNI`),
  KEY `fk_comprar_clientes` (`cliente_DNI`),
  CONSTRAINT `fk_comprar_clientes` FOREIGN KEY (`cliente_DNI`) REFERENCES `clientes` (`DNI`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_comprar_productos` FOREIGN KEY (`producto_codigo`) REFERENCES `productos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprar`
--

LOCK TABLES `comprar` WRITE;
/*!40000 ALTER TABLE `comprar` DISABLE KEYS */;
INSERT INTO `comprar` VALUES ('8903SM','73451834Y'),('4E85J7','73561249D'),('G67D69','73561249D'),('67FHK2','74894562J'),('H769KL','78975976L'),('89IT5E','79070685M');
/*!40000 ALTER TABLE `comprar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `precio_unitario` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('4E85J7','Ambientador',4.50),('67FHK2','Trajes de niño',22.15),('8903SM','Zapatos',60.89),('89IT5E','leche',3.45),('G67D69','Lavavajillas industrial',320.00),('GF36E2','Camiseta',15.50),('H769KL','Coca Cola',2.40),('HI809A','Vino tinto',12.99);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `NIF` char(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`NIF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES ('A59387645','Quémono!','C/ Aviación española 3, 1º izquierda, Madrid (Ciudad) - Madrid'),('A74825647','Grupo Euroestrellas','Plans de la Sala, 1, Sallent - Barcelona'),('A84935673','Bionitid','Carrer Colom, 586. Nave 11. Polígon Industrial Colom II, Terrassa - Barcelona');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suministrar`
--

DROP TABLE IF EXISTS `suministrar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suministrar` (
  `proveedor_NIF` char(9) NOT NULL,
  `producto_codigo` varchar(20) NOT NULL,
  PRIMARY KEY (`proveedor_NIF`,`producto_codigo`),
  UNIQUE KEY `producto_codigo` (`producto_codigo`),
  CONSTRAINT `fk_suministrar_productos` FOREIGN KEY (`producto_codigo`) REFERENCES `productos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_suministrar_proveedores` FOREIGN KEY (`proveedor_NIF`) REFERENCES `proveedores` (`NIF`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suministrar`
--

LOCK TABLES `suministrar` WRITE;
/*!40000 ALTER TABLE `suministrar` DISABLE KEYS */;
INSERT INTO `suministrar` VALUES ('A84935673','4E85J7'),('A59387645','67FHK2'),('A59387645','8903SM'),('A74825647','89IT5E'),('A84935673','G67D69'),('A59387645','GF36E2'),('A74825647','H769KL'),('A74825647','HI809A');
/*!40000 ALTER TABLE `suministrar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-29 13:16:10
