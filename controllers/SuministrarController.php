<?php

namespace app\controllers;

use app\models\Suministrar;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Productos;
use app\models\Proveedores;
use yii\helpers\ArrayHelper;

/**
 * SuministrarController implements the CRUD actions for Suministrar model.
 */
class SuministrarController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Suministrar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Suministrar::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'proveedor_NIF' => SORT_DESC,
                    'producto_codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Suministrar model.
     * @param string $proveedor_NIF Proveedor  Nif
     * @param string $producto_codigo Producto Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($proveedor_NIF, $producto_codigo)
    {
        return $this->render('view', [
            'model' => $this->findModel($proveedor_NIF, $producto_codigo),
        ]);
    }

    /**
     * Creates a new Suministrar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Suministrar();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'proveedor_NIF' => $model->proveedor_NIF, 'producto_codigo' => $model->producto_codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        $productos= Productos::find()->all();
        $listadoProductos= ArrayHelper::map($productos, 'codigo', 'nombre');
        $proveedores= Proveedores::find()->all();
        $listadoProveedores= ArrayHelper::map($proveedores, 'NIF', 'nombre');

        return $this->render('create', [
            'model' => $model,
            'listadoProductos' => $listadoProductos,
            'listadoProveedores' => $listadoProveedores,
        ]);

    }

    /**
     * Updates an existing Suministrar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $proveedor_NIF Proveedor  Nif
     * @param string $producto_codigo Producto Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($proveedor_NIF, $producto_codigo)
    {
        $model = $this->findModel($proveedor_NIF, $producto_codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'proveedor_NIF' => $model->proveedor_NIF, 'producto_codigo' => $model->producto_codigo]);
        }

        $productos= Productos::find()->all();
        $listadoProductos= ArrayHelper::map($productos, 'codigo', 'nombre');
        $proveedores= Proveedores::find()->all();
        $listadoProveedores= ArrayHelper::map($proveedores, 'NIF', 'nombre');

        return $this->render('create', [
            'model' => $model,
            'listadoProductos' => $listadoProductos,
            'listadoProveedores' => $listadoProveedores,
        ]);
    }

    /**
     * Deletes an existing Suministrar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $proveedor_NIF Proveedor  Nif
     * @param string $producto_codigo Producto Codigo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($proveedor_NIF, $producto_codigo)
    {
        $this->findModel($proveedor_NIF, $producto_codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Suministrar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $proveedor_NIF Proveedor  Nif
     * @param string $producto_codigo Producto Codigo
     * @return Suministrar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($proveedor_NIF, $producto_codigo)
    {
        if (($model = Suministrar::findOne(['proveedor_NIF' => $proveedor_NIF, 'producto_codigo' => $producto_codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
