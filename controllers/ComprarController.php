<?php

namespace app\controllers;

use app\models\Comprar;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Productos;
use app\models\Clientes;
use yii\helpers\ArrayHelper;

/**
 * ComprarController implements the CRUD actions for Comprar model.
 */
class ComprarController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Comprar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Comprar::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'producto_codigo' => SORT_DESC,
                    'cliente_DNI' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comprar model.
     * @param string $producto_codigo Producto Codigo
     * @param string $cliente_DNI Cliente  Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($producto_codigo, $cliente_DNI)
    {
        return $this->render('view', [
            'model' => $this->findModel($producto_codigo, $cliente_DNI),
        ]);
    }

    /**
     * Creates a new Comprar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comprar();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'producto_codigo' => $model->producto_codigo, 'cliente_DNI' => $model->cliente_DNI]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        $productos= Productos::find()->all();
        $listadoProductos= ArrayHelper::map($productos, 'codigo', 'nombre');
        $clientes= Clientes::find()->all();
        $listadoClientes= ArrayHelper::map($clientes, 'DNI', 'nombre');

        return $this->render('create', [
            'model' => $model,
            'listadoProductos' => $listadoProductos,
            'listadoClientes' => $listadoClientes,
        ]);
    }

    /**
     * Updates an existing Comprar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $producto_codigo Producto Codigo
     * @param string $cliente_DNI Cliente  Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($producto_codigo, $cliente_DNI)
    {
        $model = $this->findModel($producto_codigo, $cliente_DNI);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'producto_codigo' => $model->producto_codigo, 'cliente_DNI' => $model->cliente_DNI]);
        }

        $productos= Productos::find()->all();
        $listadoProductos= ArrayHelper::map($productos, 'codigo', 'nombre');
        $clientes= Clientes::find()->all();
        $listadoClientes= ArrayHelper::map($clientes, 'DNI', 'nombre');

        return $this->render('update', [
            'model' => $model,
            'listadoProductos' => $listadoProductos,
            'listadoClientes' => $listadoClientes,
        ]);
    }

    /**
     * Deletes an existing Comprar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $producto_codigo Producto Codigo
     * @param string $cliente_DNI Cliente  Dni
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($producto_codigo, $cliente_DNI)
    {
        $this->findModel($producto_codigo, $cliente_DNI)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Comprar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $producto_codigo Producto Codigo
     * @param string $cliente_DNI Cliente  Dni
     * @return Comprar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($producto_codigo, $cliente_DNI)
    {
        if (($model = Comprar::findOne(['producto_codigo' => $producto_codigo, 'cliente_DNI' => $cliente_DNI])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
