<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property string $codigo
 * @property string|null $nombre
 * @property float|null $precio_unitario
 *
 * @property Clientes[] $clienteDNIs
 * @property Comprar[] $comprars
 * @property Proveedores[] $proveedorNIFs
 * @property Suministrar $suministrar
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['precio_unitario'], 'number'],
            [['codigo'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 50],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'precio_unitario' => 'Precio Unitario',
        ];
    }

    /**
     * Gets query for [[ClienteDNIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClienteDNIs()
    {
        return $this->hasMany(Clientes::className(), ['DNI' => 'cliente_DNI'])->viaTable('comprar', ['producto_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Comprars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprars()
    {
        return $this->hasMany(Comprar::className(), ['producto_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[ProveedorNIFs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorNIFs()
    {
        return $this->hasMany(Proveedores::className(), ['NIF' => 'proveedor_NIF'])->viaTable('suministrar', ['producto_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Suministrar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuministrar()
    {
        return $this->hasOne(Suministrar::className(), ['producto_codigo' => 'codigo']);
    }
}
