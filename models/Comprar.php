<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comprar".
 *
 * @property string $producto_codigo
 * @property string $cliente_DNI
 *
 * @property Clientes $clienteDNI
 * @property Productos $productoCodigo
 */
class Comprar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comprar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto_codigo', 'cliente_DNI'], 'required'],
            [['producto_codigo'], 'string', 'max' => 20],
            [['cliente_DNI'], 'string', 'max' => 9],
            [['producto_codigo', 'cliente_DNI'], 'unique', 'targetAttribute' => ['producto_codigo', 'cliente_DNI']],
            [['cliente_DNI'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_DNI' => 'DNI']],
            [['producto_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'producto_codigo' => 'Producto',
            'cliente_DNI' => 'Cliente',
        ];
    }

    /**
     * Gets query for [[ClienteDNI]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClienteDNI()
    {
        return $this->hasOne(Clientes::className(), ['DNI' => 'cliente_DNI']);
    }

    /**
     * Gets query for [[ProductoCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoCodigo()
    {
        return $this->hasOne(Productos::className(), ['codigo' => 'producto_codigo']);
    }
}
