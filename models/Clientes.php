<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $DNI
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $direccion
 * @property string|null $fecha_de_nacimiento
 *
 * @property Comprar[] $comprars
 * @property Productos[] $productoCodigos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DNI'], 'required'],
            [['fecha_de_nacimiento'], 'safe'],
            [['DNI'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 50],
            [['apellidos'], 'string', 'max' => 80],
            [['direccion'], 'string', 'max' => 100],
            [['DNI'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DNI' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Direccion',
            'fecha_de_nacimiento' => 'Fecha De Nacimiento',
        ];
    }

    /**
     * Gets query for [[Comprars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprars()
    {
        return $this->hasMany(Comprar::className(), ['cliente_DNI' => 'DNI']);
    }

    /**
     * Gets query for [[ProductoCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoCodigos()
    {
        return $this->hasMany(Productos::className(), ['codigo' => 'producto_codigo'])->viaTable('comprar', ['cliente_DNI' => 'DNI']);
    }
}
