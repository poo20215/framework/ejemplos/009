<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property string $NIF
 * @property string|null $nombre
 * @property string|null $direccion
 *
 * @property Productos[] $productoCodigos
 * @property Suministrar[] $suministrars
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NIF'], 'required'],
            [['NIF'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 50],
            [['direccion'], 'string', 'max' => 100],
            [['NIF'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NIF' => 'Nif',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * Gets query for [[ProductoCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoCodigos()
    {
        return $this->hasMany(Productos::className(), ['codigo' => 'producto_codigo'])->viaTable('suministrar', ['proveedor_NIF' => 'NIF']);
    }

    /**
     * Gets query for [[Suministrars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuministrars()
    {
        return $this->hasMany(Suministrar::className(), ['proveedor_NIF' => 'NIF']);
    }
}
