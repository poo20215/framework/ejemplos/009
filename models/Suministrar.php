<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suministrar".
 *
 * @property string $proveedor_NIF
 * @property string $producto_codigo
 *
 * @property Productos $productoCodigo
 * @property Proveedores $proveedorNIF
 */
class Suministrar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suministrar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proveedor_NIF', 'producto_codigo'], 'required'],
            [['proveedor_NIF'], 'string', 'max' => 9],
            [['producto_codigo'], 'string', 'max' => 20],
            [['producto_codigo'], 'unique'],
            [['proveedor_NIF', 'producto_codigo'], 'unique', 'targetAttribute' => ['proveedor_NIF', 'producto_codigo']],
            [['producto_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_codigo' => 'codigo']],
            [['proveedor_NIF'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['proveedor_NIF' => 'NIF']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proveedor_NIF' => 'Proveedor  Nif',
            'producto_codigo' => 'Producto Codigo',
        ];
    }

    /**
     * Gets query for [[ProductoCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoCodigo()
    {
        return $this->hasOne(Productos::className(), ['codigo' => 'producto_codigo']);
    }

    /**
     * Gets query for [[ProveedorNIF]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorNIF()
    {
        return $this->hasOne(Proveedores::className(), ['NIF' => 'proveedor_NIF']);
    }
}
